
import { applyMiddleware, createStore } from 'redux';
import { logger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from "./reducers/index";

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['user']
};

const middlewares = [logger];

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const store = createStore(persistedReducer, applyMiddleware(...middlewares));
export const persistor = persistStore(store);

export default { store, persistor };


import {
    SAVE_INFO_USER,
    CLEAR_INFO_USER
} from '../constants/userConstants';

const initialState = {
    userInfo: []
};

const userReducer = (state = initialState, action) => {    
    switch (action.type) {
        case SAVE_INFO_USER:            
            return {
                ...state,
                userInfo: action.payload
            };
        case CLEAR_INFO_USER:
            return {
                ...state,
                userInfo: []
            }
        default:
            return state;
    }
};

export default userReducer;
import { SAVE_INFO_USER, CLEAR_INFO_USER } from '../constants/userConstants';

const saveInfoUser = payload => ({
    type: SAVE_INFO_USER,
    payload
});
const clearInfoUser = () => ({
    type: CLEAR_INFO_USER
});

export { 
    saveInfoUser,
    clearInfoUser 
};
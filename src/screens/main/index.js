import React, { useMemo, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Paper, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core'
import { useSelector, useDispatch } from 'react-redux';
import { saveInfoUser } from '../../redux/actions/userActions';
import Edit from './components/edit'
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function AcccessibleTable() {
  const classes = useStyles();
  const user = useSelector(state => state.user);
  const [data, setData] = useState([])
  const dispatch = useDispatch()
  const [open,setOpen] = useState(false)
  const [selectedIndex,setSelectedIndex] = useState()
  
  useMemo(() => {
    if (!user) return
    setData(user.userInfo)
  }, [user])

  const _deleteItem = (index) => {
    const startArr = user.userInfo.slice(0, index);
    const endArr = user.userInfo.slice(index + 1, user.userInfo.length)
    const new_arr = startArr.concat(endArr);
    dispatch(saveInfoUser(new_arr))
  }

  const _editItem = (index) => {
    setSelectedIndex(index)
    setOpen(true)
  }
  const _onCloseModal = () => {
    setOpen(false)
  }

  return (
    <>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="caption table">
        <TableHead>
          <TableRow >
            <TableCell style={{ fontWeight: 'bold' }}>Tài khoản</TableCell>
            <TableCell style={{ fontWeight: 'bold' }} align="right">Chỉnh sửa</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => (
            <TableRow key={index}>
              <TableCell component="th" scope="row">
                {row.mail}
              </TableCell>
              <TableCell align="right">
                <Grid container justifyContent="flex-end">
                  <Button onClick={() => _deleteItem(index)} variant="contained" color="secondary">Xoá</Button>
                  <Button onClick={() => _editItem(index)} variant="contained" color="primary" style={{ marginLeft: 16 }}>Sửa</Button>
                </Grid>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    <Edit open={open} onClose={_onCloseModal} selectedIndex={selectedIndex}/>
    </>
  );
}
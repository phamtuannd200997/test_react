import { FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import React, { useState } from 'react';

const FormInputPass = (props) => {
    const [showPass, setShowPass] = useState(false)
    const _onChangePass = (event) => {
      if (props.onChange) props.onChange(event)
    }
  
    const handleClickShowPassword = () => {
      setShowPass(!showPass)
    };
    const handleMouseDownPassword = (event) => {
      event.preventDefault();
    };
  
    return (
      <FormControl variant="outlined" style={{ marginTop: 32, width: '100%' }}>
        <InputLabel htmlFor="outlined-adornment-password">{props.title}</InputLabel>
        <OutlinedInput
          id="outlined-adornment-password"
          type={showPass ? 'text' : 'password'}
          value={props.value}
          onChange={(event) => _onChangePass(event)}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
              >
                {showPass ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
          labelWidth={70}
        />
      </FormControl>
    )
  }
  export default FormInputPass
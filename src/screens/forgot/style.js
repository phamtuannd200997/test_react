import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
export const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      marginTop: 32,
    },
    root: {
      width: '100%',
      height: '100vh',
      flexDirection: 'row',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
    container: {
      width: '50%',
      padding: 32,
      border: '1px solid black',
      borderRadius: 16
    },
    err: {
      marginTop: 8,
      marginBottom: -24,
      marginLeft: 8,
      color: 'red'
    },
    modal : {
      width: '30%',
      padding: 32,
      border: '1px solid black',
      borderRadius: 16
    }
  }),
);

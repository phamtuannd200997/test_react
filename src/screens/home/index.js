import { Button, Grid, TextField, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FormInputPass from './components/form-input';
import Register from './components/register';
import { useStyles } from './style';
import { useParams, useHistory } from 'react-router-dom';

export default function Home() {
  // variable
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.user);
  const [open, setOpen] = useState(false)
  const [values, setValues] = useState({
    password: "",
    email: "",
  });
  const history = useHistory();
  const [messageErr, setMessageErr] = useState("")
  const [isChange, setIsChange] = useState(false)
  // action
  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value })
  }

  const _login = () => {
    if (values.email === "") {
      setMessageErr("Email không được để trống")
      return
    }
    if (values.password === "") {
      setMessageErr("Mật khẩu không được để trống")
      return
    }
    setMessageErr("")
    let account = user.userInfo.find((item) => item.mail === values.email && item.pass === values.password)
    if (account !== undefined) {
      history.push({
        pathname: '/main',
      });
    } else {
      setMessageErr("Tài khoản hoặc mật khẩu không đúng")
    }
  }

  const _onCloseModal = () => {
    setOpen(false)
  }
  const _openModal = () => {
    setOpen(true)
  }
  console.log("values", values)

  function _test() {
    setIsChange(!isChange)
  }
  console.log("isChange", isChange)

  useEffect(() => {
    console.log("in")
    return () => {
      console.log("out")
    }
  }, [])

  // function _checkMessage(messageErr) {
  //   if (messageErr !== "") {
  //     return (
  //       <Typography className={classes.err}>{messageErr}</Typography>
  //     )
  //   }
  // }
  function _onClickGoToForgot() {
    history.push({
      pathname: '/forgot',
    })
  }

  // const data = [

  // ]

  const [data, setData] = useState([
    { account: "phamtuan", pass: "1111" },
    { account: "tuan", pass: "2222" },
  ])

  const [account, setAccount] = useState("")
  const [pass, setPass] = useState("")

  function _onChangeAccount(value) {
    setAccount(value)
  }

  function _onChangePass(value) {
    // console.log(value)
    setPass(value)
  }

  function _onRegister() {
    console.log("account : ", account)
    console.log("pass : ", pass)
    // var new_arr = data 
    // var arr = [
    //   {account :account, pass : pass}
    // ]
    // setData(new_arr.concat(arr))
    setData(data.concat([{ account: account, pass: pass }]))
  }

  // layout
  return (
    <Grid className={classes.root}>
      {/* <Grid className={classes.container}>
        <TextField
          id="outlined-basic"
          label="Email / Số điện thoại"
          variant="outlined"
          style={{ width: '100%' }}
          value={values.email}
          onChange={handleChange('email')}
        />
        <FormInputPass
          title="Mật khẩu"
          value={values.password}
          onChange={handleChange('password')}
        />
        {messageErr !== "" && <Typography className={classes.err}>{messageErr}</Typography>}
        <Grid container spacing={3} style={{ marginTop: 32 }}>
          <Grid item xs={12}>
            <Button onClick={_login} variant="contained" color="primary" style={{ width: '100%' }}>Đăng nhập</Button>
          </Grid>
        </Grid>
        <Grid className={classes.button} onClick={_openModal} container justifyContent="center">
          <Button color="primary" >Đăng ký</Button>
        </Grid>
        <Grid className={classes.button} onClick={_onClickGoToForgot} container justifyContent="center">
          <Button color="primary" >Quen mật khẩu</Button>
        </Grid>
      </Grid>

      <Register open={open} onClose={_onCloseModal} /> */}
      <p>Thêm : </p>
      <input onChange={(e) => _onChangeAccount(e.target.value)} style={{ backgroundColor: 'red', height: 50, width: 400 }} placeholder="Tai khoan" />
      <input onChange={(e) => _onChangePass(e.target.value)} style={{ backgroundColor: 'red', height: 50, width: 400 }} placeholder="Mật khẩu" />

      <Button color="primary" onClick={() => _onRegister()}>
        Đăng ký
        </Button>
      <Grid>
        <p>Sửa : </p>
        <input onChange={(e) => _onChangeAccount(e.target.value)} style={{ backgroundColor: 'red', height: 50, width: 400 }} placeholder="Tai khoan" />
        <input onChange={(e) => _onChangePass(e.target.value)} style={{ backgroundColor: 'red', height: 50, width: 400 }} placeholder="Mật khẩu" />
      </Grid>



      <Button color="primary" onClick={() => _onRegister()}>
        Edit
        </Button>

      <Grid style={{ display: 'flex', margin: 32 }}>
        <Typography style={{ width: 300 }}>Tai khoan</Typography>
        <Typography style={{ width: 300 }}>Mật khẩu</Typography>
      </Grid>

      {data.map((item, index) => {
        return (
          <Grid style={{ display: 'flex', margin: 32,alignItems : 'center' }}>
            <Typography style={{ width: 300 }}>{item.account}</Typography>
            <Typography style={{ width: 300 }}>{item.pass}</Typography>
            <Button color="primary" onClick={() => {}}>Xosa</Button>
          </Grid>
        )
      })}


    </Grid>
  )
}
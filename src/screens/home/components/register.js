import React, { memo, useState, useEffect } from 'react'
import { useStyles } from '../style'
import { Modal, Typography, Grid, Button, TextField } from '@material-ui/core'
import FormInputPass from './form-input'
import { saveInfoUser } from '../../../redux/actions/userActions'
import { useDispatch, useSelector } from 'react-redux'

const Register = (props) => {
    const classes = useStyles()
    const [values, setValues] = useState({
        password: "",
        email: "",
        rePass: ""
    });
    const user = useSelector(state => state.user);
    const dispatch = useDispatch()
    const [messageErr, setMessageErr] = useState("")

    useEffect(() => {
        setValues({
            password: "",
            email: "",
            rePass: ""
        })
        setMessageErr("")
    }, [props.open])

    const handleClose = () => {
        if (props.onClose) props.onClose()
    }
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value })
    }
    const _register = () => {
        let account = user.userInfo.find((item) => item.mail === values.email)
        if (values.email === "") {
            setMessageErr("Email không được để trống")
            return
        }
        if (values.password === "") {
            setMessageErr("Mật khẩu không được để trống")
            return
        }
        if (values.rePass === "") {
            setMessageErr("Nhập lại mật khẩu không được để trống")
            return
        }
        if (values.rePass !== values.password) {
            setMessageErr("Mật khẩu không trùng khớp")
            return
        }
        if (account !== undefined) {
            setMessageErr("Tài khoản đã được tạo")
            return
        }
        setMessageErr("")
        dispatch(saveInfoUser(user?.userInfo.concat([{
            mail: values.email,
            pass: values?.password
        }])))
        handleClose()
    }

    return (
        <Modal
            open={props.open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <Grid className={classes.root} >
                <Grid className={classes.container} style={{ backgroundColor: 'white' }}>
                    <TextField
                        id="outlined-basic"
                        label="Email / Số điện thoại"
                        variant="outlined"
                        style={{ width: '100%' }}
                        value={values.email}
                        onChange={handleChange('email')}
                    />
                    <FormInputPass
                        title="Mật khẩu"
                        value={values.password}
                        onChange={handleChange('password')}
                    />
                    <FormInputPass
                        title="Nhập lại mật khẩu"
                        value={values.rePass}
                        onChange={handleChange('rePass')}
                    />
                    {messageErr !== "" && <Typography className={classes.err}>{messageErr}</Typography>}
                    <Grid container spacing={3} style={{ marginTop: 32 }} >
                        <Grid item xs={6} container justifyContent="center">
                            <Button onClick={handleClose} variant="contained" color="secondary">Huỷ</Button>
                        </Grid>
                        <Grid item xs={6} container justifyContent="center">
                            <Button onClick={_register} variant="contained" color="primary">Đăng ký</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Modal>
    )
}

export default memo(Register)
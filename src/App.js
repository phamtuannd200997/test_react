import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import Home from './screens/home'
import Main from './screens/main'
import Forgot from './screens/forgot'

export default function App() {
  return (
    <Router>
        <Switch >
          <Route path="/main">
            <Main />
          </Route>
          <Route path="/">
            <Home />
          </Route>
          <Route path="/forgot">
            <Forgot />
          </Route>
        </Switch>
    </Router>
  );
}



